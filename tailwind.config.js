module.exports = {
    content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
    theme: {
        extend: {
            keyframes: {
                blink: {
                    "0%, 100%": {
                        opacity: 1,
                    },
                    "50%": {
                        opacity: 0,
                    },
                },
            },
            animation: {
                blink: "blink 1s ease-in-out infinite",
            },
        },
    },
    plugins: [],
};
